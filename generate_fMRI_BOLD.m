function [ BLDobs, BLDgen, BLDgen_prn, NEVgen_prn ] = generate_fMRI_BOLD( bold_params )
%============================================================
% Source: generate_fMRI_BOLD.m
% Original Author: Keith Bush (September 25, 2013)
% Modified by: Onder Hazaroglu (November 11, 2016)
% 
% Purpose: Generate and return a sequence of resting state
% fMRI BOLD using HMM model with approximately 5% NEVs.
%============================================================

minInputs = 1;
maxInputs = 1;
narginchk(minInputs,maxInputs);

if ~isstruct(bold_params),
    error('%s','Missing bold_parameter structure.');
end

%Setting the BOLD generation parameters (base signal properties)
TR = bold_params.TR;
FG = bold_params.FG;
FO = bold_params.FO;
HRF_d = bold_params.HRF_d;
SNphys = bold_params.SNphys;
SNscan = bold_params.SNscan;
ARphys = bold_params.ARphys;
ARscan = bold_params.ARscan;

NTrans = bold_params.NTrans;
noiseOn = bold_params.noiseOn;
pruneOn = bold_params.pruneOn;
percentOn = bold_params.percentOn;
                                         
% Building generation kernel
KERgen = spm_advanced_hrf(1/FG,HRF_d);
Kgen = numel(KERgen);

% Generate resting state NEVs via hmm model
NEVgen = generate_NEV_via_hmm(TR*FG+NTrans*Kgen-1);

% Convolve ANEV with the HRF Model
BLDgen = convolve_anev_roi_hrf(NEVgen,FG,HRF_d);

% Observe the ANEV Model (introduce confounds)
[NEVgen_prn,BLDgen_prn,BLDobs] = observe_roi(NEVgen,BLDgen,FG, ...
                                             FO,SNphys,SNscan, ...
                                             ARphys,ARscan, ...
                                             NTrans*Kgen-1, ...
                                             noiseOn,pruneOn,percentOn);


% Observe the ANEV Model (without confound for analysis)
[NEVgen_prn_adj,BLDgen_prn_adj,BLDobs_adj] = observe_roi(NEVgen, ...
                                                  BLDgen,FG,FO, ...
                                                  SNphys, ...
                                                  SNscan, ...
                                                  ARphys, ...
                                                  ARscan, ...
                                                  NTrans*Kgen- ...
                                                  1,false,pruneOn,percentOn);
end