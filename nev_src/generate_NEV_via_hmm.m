function [NEVgen] = generate_NEV_via_hmm(nev_size,varargin)
%============================================================
% Source: generate_NEV_via_hmm.m
% Author: Onder Hazaroglu (November 11, 2016)
% 
% Purpose: Generate and return a sequence of NEVs given the
% size of the sequence using HMM model.
%============================================================
%-Parameters
% nev_size - NEV sequence length (number of time points)
% vargargin{1} - HMM models structure (defaulted below if not
% specified

minInputs = 1;
maxInputs = 2;
narginchk(minInputs,maxInputs);

if size(varargin,2)<1,
    hmm_models.prior = [0.9500 0.0500];
    hmm_models.trans = [0.9500 0.0500; 0.9000 0.1000];
    hmm_models.obs = [0.9900 0.0100; 0.0500 0.9500];
else
    hmm_models=varargin{1};
end

% base NEV matrix
NEVgen = zeros(1,nev_size);

prior = hmm_models.prior;
transmat = hmm_models.trans;
obsmat = hmm_models.obs;

% Neural events generation
cur_hid_state = 0;
prev_hid_state = 0;

for curr_time = 1:nev_size,
    if curr_time == 1,
        if rand(1)<prior(2),
            cur_hid_state = 1;
        end
        if rand(1)<obsmat(cur_hid_state+1,2)
            NEVgen(curr_time) = 1;
        end
        prev_hid_state = cur_hid_state;
    else
        if rand(1)>transmat(prev_hid_state+1,2),
            cur_hid_state = 0;
        else
            cur_hid_state = 1;
        end
        if rand(1)<obsmat(cur_hid_state+1,2),
            NEVgen(curr_time) = 1;
        end
        prev_hid_state = cur_hid_state;
    end
end

end %function end