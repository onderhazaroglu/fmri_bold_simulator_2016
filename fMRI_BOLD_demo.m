%============================================================
% Source: fMRI_BOLD_demo.m
% Original Author: Keith Bush (September 4, 2013)
% Modufied by: Onder Hazaroglu (November 11, 2016)
%
% Purpose: Demonstrate the usage of the fMRI_BOLD_generator function.
%============================================================
clear all;

%============================================================
% Setting up the required paths
%============================================================
% SPM is a required library that must be installed prior to using the
% library. SPM can be downloaded from http://www.fil.ion.ucl.ac.uk/spm/
addpath(genpath('./spm12'));
addpath(genpath('./nev_src'));
addpath(genpath('./utility_src'));

%============================================================
% Initializing BOLD parameter structure
%============================================================
bld_params = struct('savefilename', ([]), 'TS', ([]), 'FG', ([]), 'FO', ([]),...
                    'HRF_d', ([]),'SNphys', ([]), 'SNscan', ([]),...
                     'hrfOn', ([]), 'noiseOn', ([]), 'pruneOn', ([]), ...
                     'percentOn', ([]), 'N', ([]), 'ANmu', ([]), ...
                     'ANvar', ([]), 'ANrot', ([]), 'maxLag', ([]), ...
                     'LAG', ([]));

%============================================================
% Initializing default values for BOLD parameter structure
%============================================================
bold_params.TR = 200;                    % Observed trace length (number of TRs(repetitions))
bold_params.FG = 20.0;                   % Frequency generated (Hz)
bold_params.FO = 1.0;                    % Frequency observed (Hz)
bold_params.HRF_d = 4;                   % HRF shape parameter (time-to-peek)
                                         % BLN model equivalent is
                                         % HRF_d = 4
bold_params.SNphys = 6.0;                % Physiological signal-to-noise ratio of BOLD 
                                         % (defined as power ratio)
bold_params.SNscan = 9.0;                % Scanner signal-to-noise ratio 
                                         % (defined as power ratio)
bold_params.ARphys = 0.75;               % coef. auto-regression (1-step)
bold_params.ARscan = 0.1;                % coef. auto-regression (1-step).
bold_params.noiseOn = true;             % Is the noise model applied to the observation?
bold_params.pruneOn = true;              % Is pruning applied to the observation?
bold_params.percentOn = true;            % Is the signal normalized to
                                         % percent signal change?

bold_params.NTrans = 3;                  % Number of kernel widths considered
                                         % transient signal                                                         
                                                           
%============================================================
% Seed random number explicitly
%============================================================
rng('shuffle');  

desample_rate = bold_params.FG/bold_params.FO;

[BLDobs, BLDgen, BLDgen_prn, NEV] = generate_fMRI_BOLD(bold_params);

figure(1);
clf(1);

plot(BLDgen_prn,'b-'); hold on;
plot(desample_rate:desample_rate:(bold_params.TR*bold_params.FG),BLDobs,'r-');
xlabel('number of points')
ylabel('BOLD response');
legend('BLDgen','BLDobs');